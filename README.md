### Setup
-[ ] Configure a dev and prod MYSQL RDBMS

-[ ] Copy `example-inventory.properties` into `inventory.properties`

-[ ] Copy `example-inventory.properties` into `test-inventory.properties`

-[ ] Fill out `inventory.properties` with prod credentials and `test-inventory.properties`
 with dev credentials.

-[ ] Run tests and all tests should pass.

