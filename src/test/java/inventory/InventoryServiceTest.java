package inventory;

import inventory.Repositories.MealRepository;
import inventory.Repositories.MenuRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = InventoryService.class)
@TestPropertySource(locations = "classpath:test-application.properties")
public class InventoryServiceTest {

    @Autowired
    WebApplicationContext wac;
    private MockMvc mockMvc;
    private String testData = "src/test/java/inventory/data/";

    @Autowired
    MealRepository mealRepository;

    @Autowired
    MenuRepository menuRepository;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void exampleTest() {
        String body = this.restTemplate.getForObject("/", String.class);
        assertThat(body).isEqualTo("Hello World!");
    }


    // Taken from https://stackoverflow.com/a/49598618/4955458
    public String readAll(String fileName) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(new File(fileName).getAbsolutePath()));
        return String.join("\n", lines.toArray(new String[lines.size()]));
    }
/*
    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Transactional
    @Test
    public void menuCreation0() throws Exception {
        String postBody = readAll(testData+"create-menu0.json");

        this.mockMvc.perform(post("/create-menu")
                .contentType(MediaType.APPLICATION_JSON)
                .content(postBody))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("Menu creation successful"));
    }
*/
}
