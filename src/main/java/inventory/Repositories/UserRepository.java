package inventory.Repositories;

import inventory.Tables.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    User findById(Integer id);
    User findByName(String menuName);
}
