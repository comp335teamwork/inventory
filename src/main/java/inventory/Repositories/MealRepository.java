package inventory.Repositories;

import inventory.Tables.Meal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MealRepository extends CrudRepository<Meal, Long> {
    List<Meal> findByRestaurant(String restaurant);
    List<Meal> findByMenu_MenuIDAndMealName(Integer menuID, String mealName);
    Meal findByMenu_MenuIDAndId(Integer menuID, Integer id);
    Meal findByMealNameAndRestaurantAndId(String mealName, String restaurant, Integer ID);
    Meal findById(Integer ID);
    Meal findByMealToken(String mealToken);
    List<Meal> findByRestaurantAndAvailability(String restaurant, boolean availability);


}
