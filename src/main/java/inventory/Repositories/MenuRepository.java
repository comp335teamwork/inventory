package inventory.Repositories;

import inventory.Tables.Menu;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MenuRepository extends CrudRepository<Menu, Long> {
    Menu findByMenuID(Integer menuid);
    List<Menu> findByMenuName(String menuName);
    Menu findByMenuToken(String menuToken);
}


