package inventory.Tables;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class Menu {
    @Id
    @GeneratedValue()
    @NotNull
    private Integer menuID;
    @Column(unique = true)
    private String menuToken = "menu-"+ UUID.randomUUID().toString();
    private String menuName;
    private String restaurant;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @NotNull
    @JoinColumn(name="menu")
    private List<Meal> mealList = new ArrayList<>();

    public Menu() {
    }

    public Menu(String menuName, String restaurant, List<Meal> mealList) {
        this.mealList = mealList;
        this.menuName = menuName;
        this.restaurant = restaurant;
    }

    public void setMenuID(Integer menuID) {
        this.menuID = menuID;
    }

    public String getMenuToken() {
        return menuToken;
    }

    public void setMenuToken(String mealToken) {
        this.menuToken = mealToken;
    }

    public Integer getMenuID() {
        return menuID;
    }

    public List<Meal> getMealList() {
        return mealList;
    }

    public void setMealList(List<Meal> mealList) {
        this.mealList = mealList;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }




    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

}