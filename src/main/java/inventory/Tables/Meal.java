package inventory.Tables;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
public class Meal {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(unique = true)
    private String mealToken = "meal-"+ UUID.randomUUID().toString();
    private String mealName;
    private BigDecimal cost;
    private String restaurant;
    private String description;
    private boolean availability;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="menuid")
    private Menu menu;

    public Meal() {}

    public Meal(String mealName, String restaurant, BigDecimal cost, String description, boolean availability) {
        this.mealName = mealName;
        this.cost = cost;
        this.restaurant = restaurant;
        this.description = description;
        this.availability = availability;
    }

    public String getMealToken() {
        return mealToken;
    }

    public void setMealToken(String mealToken) {
        this.mealToken = mealToken;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public String getMealName() {
        return mealName;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }
}
