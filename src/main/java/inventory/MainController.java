package inventory;

import inventory.Repositories.MealRepository;
import inventory.Repositories.MenuRepository;
import inventory.Repositories.UserRepository;
import inventory.Tables.Meal;
import inventory.Tables.Menu;
import inventory.Tables.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

@RestController
class MainController {
    private final UserRepository userRepository;
    private final MenuRepository menuRepository;
    private final MealRepository mealRepository;

    @Autowired
    public MainController(UserRepository userRepository, MenuRepository menuRepository, MealRepository mealRepository) {
        this.userRepository = userRepository;
        this.menuRepository = menuRepository;
        this.mealRepository = mealRepository;
    }

     //Create menu
    @PostMapping(path = "/create-menu")
    public @ResponseBody
    Map<String, Object> createMenu(@RequestBody Menu menu) {
        menuRepository.save(menu);
        Menu create = menuRepository.findByMenuToken(menu.getMenuToken());
        Map<String, Object> creator = new HashMap<>();
        creator.put("menuToken",create);
        return creator;
    }

/*
    //Create menu
    @PostMapping(path = "/create-menu")
    public @ResponseBody
    String createMenu(@RequestBody Menu menu) {
        menuRepository.save(menu);
        return "Menu creation successful";
    }
*/


    //Delete menu -> Delete where menuID
    @PostMapping(path="/delete-menu")
    public @ResponseBody
    String deleteMenuByID(@RequestBody Menu menu) {
        Menu delete = menuRepository.findByMenuID(menu.getMenuID());
        menuRepository.delete(delete);
        return delete.getMenuName()+" is now deleted";
    }


    //Lists all menus
    @GetMapping(path = "/menu/all")
    public @ResponseBody
    Iterable<Menu> getAllMenus() {
        System.out.println();
        return menuRepository.findAll();
    }

    //Lists all meals from restaurant (findByRestaurant)
    @PostMapping(path = "menu-list/restaurant/all")
    public @ResponseBody
    Iterable<Meal> getMealsFromRestaurant(@RequestBody Meal meal) {
        System.out.println();
        return mealRepository.findByRestaurant(meal.getRestaurant());
    }

    // Add Meal to menu -> Update mealList
    @PostMapping(path="/menu-list/add-meal")
    public @ResponseBody
    String addToMenu(@RequestBody Menu menu) {
        Menu adder = menuRepository.findByMenuToken(menu.getMenuToken());
        String added ="";
        for(int i = 0; i < menu.getMealList().size() ;i++) { // adds to menuList where it access menu through menuid
            menu.getMealList().get(i).setRestaurant(adder.getRestaurant());
            adder.getMealList().add(menu.getMealList().get(i));
            added += menu.getMealList().get(i).getMealName();
        }
        menuRepository.save(adder);
        return added + " is inserted";
    }

    //Find meal by meal-token
    @PostMapping(path ="/menu-list/get-meal/meal-token")
    public @ResponseBody
    Meal getMealByToken(@RequestBody Meal meal){
        Meal get = mealRepository.findByMealToken(meal.getMealToken());
        return get;
    }

    //Find menu by menu-token
    @PostMapping(path ="/menu-list/get-menu/menu-token")
    public @ResponseBody
    Menu getMenuByToken(@RequestBody Menu menu) {
        Menu get = menuRepository.findByMenuToken(menu.getMenuToken());
        return get;
    }

    //Find meal in list where availability = true from restaurant
    @PostMapping(path = "/menu-list/availability-true")
    public @ResponseBody
    List<Meal> getMealAvailable(@RequestBody Meal meal) {
        List<Meal> got = mealRepository.findByRestaurantAndAvailability(meal.getRestaurant(),true);
        return got;
    }

    //Find meal in list where availability = false from restaurant
    @PostMapping(path = "/menu-list/availability-false")
    public @ResponseBody
    List<Meal> getMealNotAvailable(@RequestBody Meal meal) {
        List<Meal> got = mealRepository.findByRestaurantAndAvailability(meal.getRestaurant(),false);
        return got;
    }

    //Delete from menu -> Update mealList
    @PostMapping(path="/menu-list/delete/idnameres")
    public @ResponseBody
    String deleteFromMenu(@RequestBody Meal meal) {
        Meal delete = mealRepository.findByMealNameAndRestaurantAndId(meal.getMealName(),meal.getRestaurant(),meal.getId());
        mealRepository.delete(delete);
        return delete.getMealName()+" has been deleted from menu";
    }

    //Delete meal by meal-token
    @PostMapping(path ="/menu-list/delete-meal/meal-token")
    public @ResponseBody
    String deleteMealByToken(@RequestBody Meal meal){
        Meal get = mealRepository.findByMealToken(meal.getMealToken());
        mealRepository.delete(get);
        return get.getMealName()+" has been deleted";
    }

    //-------------------------USER-----------------------------
    //Finds users by ID
    @PostMapping(path="/users/findByID")
    public @ResponseBody
    String findUserID(@RequestBody User user){
        return userRepository.findById(user.getId()).getName();
    }

    //Finds userID by name
    @PostMapping(path="/users/findByName")
    public @ResponseBody
    Integer findUserIDByName(@RequestBody User user) {
        return userRepository.findByName(user.getName()).getId();
    }

    @PostMapping(path = "/users/add")
    public @ResponseBody
    String addNewUser(@RequestBody User user) {
        userRepository.save(user);
        return "Saved";
    }

    @GetMapping(path = "/all")
    public @ResponseBody
    Iterable<User> getAllUsers() {
        System.out.println();
        return userRepository.findAll();
    }

//-------------------HelloWorldTest----------------------

    @GetMapping(path = "/")
    public @ResponseBody
    String defaultPage() {
        return "Hello World!";
    }


}
